
window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        const gameContainerElement = document.getElementById('gameContainer');
        let text = '';
        let symbolCounter = 0;
        let theGameTimer = null;
        let waitGameTimer = null;
        const socket = io.connect('http://localhost:3000');

        console.log('connected');
        socket.emit('joinGameAttempt', { token: jwt });

        socket.on('startTimerBeforeTheGame', payload => {
            const timeRemaining = moment(payload.startTime).diff(moment(), 'second');

            if(timeRemaining < 0) {
                socket.emit('joinWaitRoom', { token: jwt });
            }
            else {
                const timerElement = renderTimer(payload.message, timeRemaining);
                gameContainerElement.prepend(timerElement);
    
                socket.emit('joinGame', { token: jwt });

                reduceBeforeTheGameTimer({ 
                    message: 'The game starts in', 
                    login: payload.login,
                    gameDuration: payload.gameDuration,
                    timeRemaining
                });
            }

        });

        socket.on('getGameDurationTime', payload => {
            const timeRemaining = moment(payload.gameDuration).diff(moment(), 'second');

            console.log(timeRemaining);

            const timerElement = renderTimer(payload.message, timeRemaining);
            gameContainerElement.prepend(timerElement);

            reduceWaitGameTimer({
                message: payload.message,
                timeRemaining
            });
        });

        socket.on('sendTextId', payload => {
            const textId = payload.textId;
            fetch(`/text/${textId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${jwt}` 
                },
                }).then(res => {
                    res.json().then(body => {  
                        text = body.text;
                        const textElement = renderText(text);
                        gameContainerElement.prepend(textElement);
                        socket.emit('getTextLength', { token: jwt, textLength: text.length} );
                        console.log("get text" + " " + text);
                    })
                }).catch(err => {
                    console.log(err);
                    console.log('request went wrong');
                });
        });

        socket.on('joinNewPlayer', payload => {
            console.log(`I know that new player with login ${payload.userProgress.login} has joined`);
            const playersContainerElement = document.getElementById('players');

            const updatedPlayersContainerElement = createElement({
                tagName: 'div', 
                className: 'players',
                attributes: {
                    id: 'players'
                }
            });

            Object.keys(payload.userProgress).forEach(user => {
                const playerBar = renderPlayerBar({
                    playerName: user,
                    progress: payload.userProgress[user].progress,
                    textLength: payload.userProgress[user].textLength
                });
                updatedPlayersContainerElement.appendChild(playerBar);
            });

            if(playersContainerElement) {
                playersContainerElement.replaceWith(updatedPlayersContainerElement);
            }
            else {
                gameContainerElement.appendChild(updatedPlayersContainerElement);
            }
 
        });

        function renderPlayerBar({ playerName, progress, textLength }) {
            const playerElement = createElement({tagName: 'div', className: 'player'});
            const playerNameElement = createElement({tagName: 'span', className: 'player-name'});
            const playerProgressElement = createElement({
                tagName: 'progress', 
                className: 'player-progress',
                attributes: {
                    max: textLength,
                    value: progress,
                    id: `${playerName}Progress`
                }
            });
            playerElement.innerHTML = playerName;
            playerElement.append(playerNameElement, playerProgressElement);
    
            return playerElement;
        }

        function reduceBeforeTheGameTimer({ message, timeRemaining, gameDuration, login}) {
            const timerElement = document.getElementById('timer');

            const beforeTheGameTimer = setInterval(() => {
                timerElement.innerHTML = `${message}: ${timeRemaining}s`;
                console.log('tik before the game ' + timeRemaining);
                timeRemaining--;
                if(timeRemaining < 0) {
                    clearInterval(beforeTheGameTimer);
                    startGame('The game ends in', gameDuration, login);
                }
            }, 1000);
        }

        function startGame(message, timeRemaining, login) {
            console.log("Game started");

            const resultListElement = document.getElementById('result');
            console.log(resultListElement);
            if(resultListElement) {
                resultListElement.parentElement.removeChild(resultListElement);
                console.log("Remove results");
            }

            const timerElement = document.getElementById('timer');
            timerElement.innerHTML = `${message}: ${timeRemaining}s`;

            window.addEventListener('keypress', event => enterSymbolHandler(event, login));

            reduceTheGameTimer({
                message, 
                timeRemaining
            });
        }

        function reduceWaitGameTimer({ message, timeRemaining }) {
            const timerElement = document.getElementById('timer');
            waitGameTimer = setInterval(() => {
                timerElement.innerHTML = `${message}: ${timeRemaining}s`;
                console.log('wait tik ' + timeRemaining);
                timeRemaining--;
                if(timeRemaining < 0) {
                    clearInterval(waitGameTimer);
                    clearInterval(theGameTimer);
                    socket.emit('joinGameAttempt', { token: jwt });
                }
            }, 1000);
        }

        socket.on('endGame', payload => {
            console.log('finish waiting game');
            clearInterval(waitGameTimer);
            clearInterval(theGameTimer);
            while (gameContainerElement.firstChild) {
                gameContainerElement.removeChild(gameContainerElement.firstChild);
            }

            socket.emit('joinGameAttempt', { token: jwt });
        });

        function reduceTheGameTimer({ message, timeRemaining }) {
            const timerElement = document.getElementById('timer');
            theGameTimer = setInterval(() => {
                timerElement.innerHTML = `${message}: ${timeRemaining}s`;
                console.log('tik in the game ' + timeRemaining);
                timeRemaining--;
                if(timeRemaining < 0) {
                    clearInterval(theGameTimer);
                    clearInterval(waitGameTimer);
                    socket.emit('finishTime', { token: jwt });
                }
            }, 1000);
        }
    
        socket.on('getResult', payload => {
            console.log('get result list');
            symbolCounter = 0;
            clearInterval(theGameTimer);
            clearInterval(waitGameTimer);
            while (gameContainerElement.firstChild) {
                gameContainerElement.removeChild(gameContainerElement.firstChild);
            }

            const resultListElement = renderResult(payload.results);
            gameContainerElement.appendChild(resultListElement);

            socket.emit('joinGameAttempt', { token: jwt });
        });

        socket.on('updateProgress', payload => {
            const progressElement = document.getElementById(`${payload.login}Progress`);
            progressElement.value++;
        });

        socket.on('playerDisconnected', payload =>{
            const playersContainerElement = document.getElementById('players');

        });

        function renderText(text) {
            const textArray = text.split('');
            const textElement = createElement({
                tagName: 'p', 
                className: 'text',                
                attributes: {
                    id: 'text'
                }
            });
            textArray.forEach((symbol) => {
                const symbolElement = createElement({
                    tagName: 'span', 
                    className: 'symbol',
                });
                symbolElement.innerHTML = symbol
                textElement.append(symbolElement);
            });
    
            return textElement;
        }
    
        function renderTimer(message, timeRemaining) {
            const timerElement = createElement({
                tagName: 'p', 
                className: 'timer',
                attributes: {
                    id: 'timer'
                }
            });
            timerElement.innerHTML = `${message}: ${timeRemaining}s`;

            return timerElement;
        }


        function renderResult(score) {
            const resultLabelElement = createElement({ tagName: 'h3', className: 'result-label'});
            resultLabelElement.innerHTML = 'Final score';
            const resultListElement = createElement({ 
                tagName: 'ol', 
                className: 'result',
                attributes: {
                    id: 'result'
                }
            });

            score.forEach(player => {
                const resultListItemElement = createElement({ tagName: 'li', className: 'result-item'});
                resultListItemElement.innerHTML = player;
                resultListElement.appendChild(resultListItemElement);
            });

            resultListElement.prepend(resultLabelElement);
            return resultListElement;
        }

        function enterSymbolHandler(event, playerName) {
            const progressElement = document.getElementById(`${playerName}Progress`);
            const textElement = document.getElementById('text');

            if(event.which != 0 && event.charCode != 0) {
                const symbolArray = [...textElement.children];
                if(event.which < 32) return null;

                const enteredSymbol = String.fromCharCode(event.which);
                console.log(enteredSymbol);
                if(enteredSymbol === symbolArray[symbolCounter].innerHTML) {
                    progressElement.value++;
                    symbolArray[symbolCounter].classList.add('entered');
                    socket.emit('enteredSymbol', { token: jwt })
                    symbolCounter++;
                }
            }
        }
    
        function createElement({ tagName, className = '', attributes = {} }) {
            const element = document.createElement(tagName);
            element.classList.add(className);
            Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
    
            return element;
        }
    }
}



