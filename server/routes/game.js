const express = require('express');
const passport = require('passport');
const router = express.Router();
const path = require('path');
const repo = require('../repositories/repositories');


router.get('/game', (req, res) => {
    res.sendFile(path.join(__dirname, '..', '..', 'client', 'game.html'));
});

router.get('/text/:id', passport.authenticate('jwt',  { session: false }), (req, res) => {
    console.log(req.params.id);
    const textObj = repo.Data.getText(+req.params.id);
    res.status(200).json(textObj);
});


module.exports = router;