const path = require('path');
const moment = require('moment');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const passport = require('passport');
const bodyParser = require('body-parser');
const loginRoutes = require('./server/routes/login');
const gameRoutes = require('./server/routes/game');
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');

require(path.join(__dirname, 'server' ,'passport.config'));
app.use(passport.initialize());

app.use(express.static(path.join(__dirname, 'client')));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'client', 'index.html'));
});

app.use('/', loginRoutes);
app.use('/', gameRoutes);



let initiatedAt = null;
let isInitiated = false;

const userProgress = {};
let results = null;


io.on('connection', socket => {
    const textId = Math.floor(Math.random() * 4) + 1; 
    // const textId = 11;
    
    socket.on('joinGameAttempt', payload => {
        console.log('connected user');

        if(socket.room !== undefined) {
            console.log('left the room ' + socket.room);
            socket.leave(socket.room);
        }

        if(!isInitiated) {
            initiatedAt = moment().add(10, 'second');
            isInitiated = true;
            console.log('set startTime');
        }

        const gameDuration = 60;

        const user = jwt.verify(payload.token, 'someSecret');
        if(user) {

            socket.emit('startTimerBeforeTheGame', { 
                message: 'The game starts in', 
                login: user.login,
                startTime: initiatedAt,
                gameDuration
            });
        }
        else {
            console.log('not verified');
        }
        
    });


    socket.on('joinGame', payload => {
        const user = jwt.verify(payload.token, 'someSecret');
        if(user) {
            socket.emit('sendTextId', { textId });

            console.log('send text id');
        }
        else {
            console.log('not verified');
        }
    });

    
    socket.on('getTextLength', payload => {
        const user = jwt.verify(payload.token, 'someSecret');
        if(user) {
            userProgress[user.login] = {
                progress: 0,
                textLength: payload.textLength
            };

            socket.join('gameRoom');
            socket.room = 'gameRoom';
            io.sockets.in('gameRoom').emit('joinNewPlayer', { userProgress });
            console.log('Join to the game group');
        }
        else {
            console.log('not verified');
        }
    });

    socket.on('joinWaitRoom', payload => {

        const user = jwt.verify(payload.token, 'someSecret');
        if(user) {
            socket.join("waitRoom");
            socket.room = 'waitRoom';
            io.sockets.in('waitRoom').emit('getGameDurationTime', { 
                message: 'Game has already started. You will be able to join the game in', 
                gameDuration: initiatedAt.add(42, 'second') 
            });
            console.log('Join to the wait group');
        }
        else {
            console.log('not verified');
        }
    });

    socket.on('enteredSymbol', payload => {
        const user = jwt.verify(payload.token, 'someSecret');
        if(user) {
            console.log('Updated progress');
            socket.broadcast.to('gameRoom').emit('updateProgress', { login: user.login });

            userProgress[user.login].progress++;
            const isPlayerFinish = userProgress[user.login].progress === userProgress[user.login].textLength;
            // if(isPlayerFinish) {
            //     results.unshift(user.login);
            // }

            results = Object.keys(userProgress).sort((player, anotherPlayer) => userProgress[anotherPlayer].progress - userProgress[player].progress);

            const isGameFinish = Object.keys(userProgress).every(user => userProgress[user].progress === userProgress[user].textLength);

            if (isGameFinish) {
                isInitiated = false;
                io.sockets.in('gameRoom').emit('getResult', { results });
                io.sockets.in('waitRoom').emit('endGame');
            }
        }
        else {
            console.log('not verified');
        }
    });

    socket.on('disconnect', payload => {
        socket.emit('playerDisconnected');
      });
    
    socket.on('finishTime', payload => {
        console.log('finish time');
        const user = jwt.verify(payload.token, 'someSecret');
        if(user) {
            isInitiated = false;
            io.sockets.in('gameRoom').emit('getResult', { results });
            io.sockets.in('waitRoom').emit('endGame');
        }
        else {
            console.log('not verified');
        }
    });

});

server.listen(3000);

